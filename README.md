# ABOUT #
There're some addons for Albert. 
Documentation: https://albertlauncher.github.io/docs/extending/external/

# Plugin: Kill Util #

## Install ##
```
git clone https://pandozer@bitbucket.org/pandozer/albert-addons.git
cp albert-addons/external/albert.py $HOME/.local/share/albert/external/
cp albert-addons/external/terminal.png $HOME/.local/share/albert/external/
cp albert-addons/external/kill.py $HOME/.local/share/albert/external/
chmod +x $HOME/.local/share/albert/external/kill.py
```

### Usage ###
* Open Albert.
* Type "kill " without quotes.
* Next words will filter the result.

# Plugin: Clipboard Manager #
## Install ##
```
sudo apt install python3-pyperclip xdotool
git clone https://pandozer@bitbucket.org/pandozer/albert-addons.git
cp albert-addons/external/albert.py $HOME/.local/share/albert/external/
cp albert-addons/external/clipboard.png $HOME/.local/share/albert/external/
cp albert-addons/external/clipboard-manager.py $HOME/.local/share/albert/external/
chmod +x $HOME/.local/share/albert/external/clipboard-manager.py
```

## Usage ##
* Open Albert.
* Type "p " without quotes.
* Next words will filter the result.

## Bad ##
* Now Albert doesn't support sorting result.
* If you want to bind keyboard shortcut use script "bind-clipboard-manager.sh" (don't forget to make it executable)

# Theme: NumixLight #

## Install ##
```
git clone https://pandozer@bitbucket.org/pandozer/albert-addons.git
sudo cp albert-addons/external/NumixLight.qss /usr/share/albert/themes/
```
