#!/bin/sh

albert toggle

pids=$(xdotool search --class "albert")
for pid in $pids; do
    name=$(xdotool getwindowname $pid)
    case "$name" in "albert — Albert") 
    	$(xdotool type --window $pid "p ");;
    esac
done

