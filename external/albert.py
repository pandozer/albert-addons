#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os


class Plugin():
    """
        Simple class for albert external plugins
        See documentation: https://albertlauncher.github.io/docs/extending/external/
    """

    def __init__(self, metadata):
        self.meta = metadata
        self.items = []
        self.albert_query = ''
        self.albert_op = ''

        if 'ALBERT_QUERY' in os.environ:
            trigger = self.meta['trigger']
            self.albert_query = os.environ['ALBERT_QUERY'][len(trigger):]

        if 'ALBERT_OP' in os.environ:
            self.albert_op = os.environ['ALBERT_OP']

    def run(self):
        if self.albert_op in ['INITIALIZE', 'METADATA', 'SETUPSESSION', 'QUERY', 'TEARDOWNSESSION', 'FINALIZE']:
            getattr(self, self.albert_op.lower())()

    def metadata(self):
        self.print_as_json(self.meta)

    def query(self):
        query = self.albert_query.lower()
        filtered_items = [item for item in self.items if query in item['name'].lower()]

        self.print_as_json({
            'items': filtered_items
        })

    def initialize(self):
        pass

    def setupsession(self):
        pass

    def teardownsession(self):
        pass

    def finalize(self):
        pass

    def new_item(self, name, desc, icon, actions):
        return {
            'id': 'extension.wide.unique.id.' + self.meta['name'],
            'name': name,
            'description': desc,
            'icon': icon,
            'actions': actions
        }

    def new_action(self, name, command, args):
        return {
            'name': name,
            'command': command,
            'arguments': args
        }

    def print_as_json(self, obj):
        print(json.dumps(obj, indent=4), end='')
