#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import albert

SCRIPT = os.path.realpath(__file__)
ICON = os.path.dirname(SCRIPT) + os.path.sep + 'terminal.png'

if __name__ == "__main__":
    metadata = {
        'iid': 'org.albert.extension.external/v2.0',
        'name': 'Kill Util',
        'version': '1.0',
        'author': 'Sayplz',
        'dependencies': [],
        'trigger': 'kill '
    }

    plugin = albert.Plugin(metadata)

    if 'QUERY' in plugin.albert_op:
        for line in os.popen('ps -A'):
            pid, _, _, name, *args = line.split()
            desc = 'pkill -f ' + name

            action0 = plugin.new_action(desc, 'pkill', ['-f', name])
            item = plugin.new_item(name, desc, ICON, [action0])
            if item not in plugin.items:
                plugin.items.append(item)

    plugin.run()
