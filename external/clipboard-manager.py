#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import time
import pyperclip
import struct
import subprocess
import albert

# Settings
TRIGGER = 'p '
PASTE_KEY = 'ctrl+v'
CLIP_LIMIT = 50
STRING_LIMIT = 100
DAEMON_DELAY = 1

HISTORY_FILE = os.environ['HOME'] + os.path.sep + '.clipboard-history'
SCRIPT = os.path.realpath(__file__)
ICON = os.path.dirname(SCRIPT) + os.path.sep + 'clipboard.png'


class ClipboardManager(albert.Plugin):
    def initialize(self):
        open(HISTORY_FILE, "a+").close()
        if os.fork() == 0:
            self.daemon()

    def query(self):
        clips = self.read()

        for index, clip in enumerate(clips):
            clip = clip.strip().replace('\n', ' ').replace('  ', ' ')
            name = clip[0:STRING_LIMIT]
            desc = 'Paste ' + name
            action0 = self.new_action(desc, SCRIPT, ['paste', str(index)])
            item = self.new_item(name, desc, ICON, [action0])
            self.items.append(item)

        super().query()

    def paste(self, index):
        clips = self.read()
        pyperclip.copy(clips[int(index)])
        time.sleep(0.1)
        subprocess.Popen(['xdotool', 'key', PASTE_KEY], stdin=subprocess.PIPE)

    def daemon(self):
        clips = self.read()
        while True:
            clip = pyperclip.paste()
            if clip and (not clips or clip != clips[0]):
                if clip in clips:
                    clips.remove(clip)
                clips.insert(0, clip)
                self.write(clips[0:CLIP_LIMIT])
            time.sleep(DAEMON_DELAY)

    def read(self):
        result = []
        with open(HISTORY_FILE, 'rb') as file:
            bytes_read = file.read(4)
            while bytes_read:
                chunksize = struct.unpack('>i', bytes_read)[0]
                bytes_read = file.read(chunksize)
                result.append(bytes_read.decode('utf-8'))
                bytes_read = file.read(4)
        return result

    def write(self, items):
        with open(HISTORY_FILE, 'wb') as file:
            for item in items:
                item = item.encode('utf-8')
                item = struct.pack('>i', len(item)) + item
                file.write(item)


if __name__ == "__main__":
    metadata = {
        'iid': 'org.albert.extension.external/v2.0',
        'name': 'Clipboard Manager',
        'version': '1.0',
        'author': 'Sayplz',
        'dependencies': ['xdotool', 'python3-pyperclip'],
        'trigger': TRIGGER
    }

    cm = ClipboardManager(metadata)

    if len(sys.argv) > 1 and sys.argv[1] == 'paste':
        cm.paste(sys.argv[2])
    else:
        cm.run()
